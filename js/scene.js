function animate() {
  requestAnimationFrame(animate);
  cube.rotation.x += 0.01;
  cube.rotation.y += 0.01;
  renderer.render(scene, camera);
}

// colors changing
var params = {
	transparent: false,
    color: 0xff00ff
};
var gui = new dat.GUI();
var folder = gui.addFolder( 'MATERIAL' );
folder.addColor( params, 'color' ).onChange( function() { cube.material.color.set( params.color ); } );
folder.add(params, 'transparent').onChange(function() {if (params.transparent) {cube.material.opacity = 0.5; cube.material.transparent=true;} else {cube.material.transparent=false;}});
folder.open();


var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);

var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight, false);
document.body.insertBefore(renderer.domElement, document.body.firstChild);

var geometry = new THREE.BoxGeometry();
var material = new THREE.MeshBasicMaterial({ color: 0xffffff });
var cube = new THREE.Mesh(geometry, material);
scene.add(cube);

camera.position.z = 5;

animate();
